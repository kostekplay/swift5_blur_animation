////  ViewController.swift
//  Swift5BlurAnimation
//
//  Created on 21/10/2020.
//  
//

import UIKit

class ViewController: UIViewController {
    
    private let image: UIImageView = {
        let i = UIImageView(image: UIImage(named: "background"))
        i.contentMode = .scaleAspectFill
        i.clipsToBounds = true
        return i
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(image)
        image.frame = view.bounds
        
        createBlur()
    }
    
    private func createBlur() {
     
        let blueEffect = UIBlurEffect(style: .regular)
        let visualEffectView = UIVisualEffectView(effect: blueEffect)
        visualEffectView.frame = view.bounds
        visualEffectView.alpha = 0
        view.addSubview(visualEffectView)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            UIView.animate(withDuration: 1) {
                visualEffectView.alpha = 1
            }
        }
    }
}

